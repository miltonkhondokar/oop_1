
<!DOCTYPE html>
<head>
<title>database_test</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<!-- CSS Link -->
<link rel="stylesheet" type="text/css" href="css/style.css">
<!-- Bootstrap -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!-- font-awesome -->
<link rel="stylesheet" href="css/font-awesome.min.css">
</head>
<body>
<div class="container">
<!-- Here Starts Second row -->
<div class="row">
<!-- Here starts Second column -->





<div class="col-sm-6 col-md-6 col-lg-6">
<h4>Student Details</h4>
<div class="table-responsive">

<form action="info.php" method="post">
<table class="table table-bordered">
<thead class="th">
<tr>
<th>Details</th>
<th>Input Fields</th>
</tr>
</thead>
<tbody>

<tr>
<td>Name</td>
<td><input type="text" id="textfield1" class="text" name="std_name" value="" placeholder="Name" title="Enter Your Name"  ><input type="hidden" name="txtid" value="0"></td>
</tr>

<tr>
<td>Mobile Number</td>
<td><input type="text" id="textfield2" class="text" name="std_mobile" value="" placeholder="01xxxxxxxxx" title="Enter Your Mobile Number"  ></td>
</tr>

<tr>
<td>Email</td>
<td><input type="email" id="textfield3" class="text" name="std_email" value="" placeholder="example@mail.com" title="Enter Your Email Address"  ></td>
</tr>

<tr>
<td>Present Address</td>
<td><input type="text" id="textfield4" class="text" name="std_pa" value="" placeholder="Present Address" title="Enter Your Present Address"  ></td>
</tr>
<tr>

<td>Home Town</td>
<td><input type="text" id="textfield5" class="text" name="std_dist" value="" placeholder="Home Town" title="Enter Your Home Town"  ></td>
</tr>

<tr>
<td></td>
<td><button type="button"  class="btn btn-warning" onclick="clearFields()">Clear</button>
<input type="submit" name="btn-std" class="btn btn-success" value="Submit" />
</td>
</tr>
</tbody>
</table>
</form>
</div>
</div>
<!-- Here ends Second column -->




<div class="col-sm-6 col-md-6 col-lg-6">
<h4>Lab Details</h4>
<div class="table-responsive">

<form action="info.php" method="post">
<table class="table table-bordered">
<thead class="th">
<tr>
<th>Details</th>
<th>Input Fields</th>
</tr>
</thead>
<tbody>

<tr>
<td>LAB Desk</td>
<td><input type="text" id="tf1" class="text" name="lab_desk" value="" placeholder="LAB Desk" title="Enter Your LAB Desk No"  ><input type="hidden" name="txtid" value="0"></td>
</tr>

<tr>
<td>LAB Chair</td>
<td><input type="text" id="tf2" class="text" name="lab_chair" value="" placeholder="LAB Chair" title="Enter Your LAB Chair No"  ></td>
</tr>

<tr>
<td>LAB PC</td>
<td><input type="text" id="tf3" class="text" name="lab_pc" value="" placeholder="LAB PC" title="Enter Your LAB PC No"  ></td>
</tr>

<tr>
<td>LAB Projector</td>
<td><input type="text" id="tf4" class="text" name="lab_projector" value="" placeholder="LAB Projector" title="Enter Your LAB Projector Name"  ></td>
</tr>
<tr>

<td>LAB AC</td>
<td><input type="text" id="tf5" class="text" name="lab_ac" value="" placeholder="LAB AC" title="Enter Your LAB AC Quantity"  ></td>
</tr>

<tr>
<td></td>
<td><button type="button"  class="btn btn-warning" onclick="clearFields_2()">Clear</button>
<input type="submit" name="btn-std" class="btn btn-success" value="Submit" />
</td>
</tr>
</tbody>
</table>
</form>
</div>
</div>
<!-- Here ends Second column -->
</div>
<!-- Here ends Second row -->	
<!-- =============================================== -->
<script type="text/javascript">
function clearFields() {
document.getElementById('textfield1').value="";
document.getElementById('textfield2').value="";
document.getElementById('textfield3').value="";
document.getElementById('textfield4').value="";
document.getElementById('textfield5').value="";
}

function clearFields_2() {
document.getElementById('tf1').value="";
document.getElementById('tf2').value="";
document.getElementById('tf3').value="";
document.getElementById('tf4').value="";
document.getElementById('tf5').value="";
}
</script>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
</body>
</html>